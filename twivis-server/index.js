
/**
 * Module dependencies.
 */

var inherits = require('util').inherits;
var EventEmitter = require('events').EventEmitter;

/**
 * Dummy server constructor.
 */

function Server() {
  if (!(this instanceof Server)) { return new Server(); }
  this.timer = null;
}

inherits(Server, EventEmitter);

// query
Server.prototype.query = function() {
  var _this = this;
  this.timer = setInterval(function() {
    _this.emit('data', {
      status: 'hoge'
    });
  }, 100);
  return this;
};

module.exports = Server;
