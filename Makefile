
C8 = ./node_modules/.bin/component
SRC = $(shell find lib -name "*.js" ! -name ".\#*.js")
APP = /tmp/twivis.nw

run: build
	@rm -f $(APP)
	zip -rq $(APP) *
ifeq ($(shell uname), Linux)
	/usr/lib/node_modules/nodewebkit/nodewebkit/nw $(APP)
else
	open -n -a node-webkit $(APP)
endif

build: node_modules components $(SRC)
	@$(C8) build --standalone twivis

components:
	@$(C8) install

node_modules: package.json
	@npm install

clean:
	rm -fr build

distclean: clean
	rm -fr components

.PHONY: clean distclean run
