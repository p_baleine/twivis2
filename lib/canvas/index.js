
/**
 * Module dependencies.
 */

var d3 = require('d3');
var drawTweet = require('draw-tweet');
var canvasUtil = require('canvas-util');

/**
 * Max tweets count
 */

var maxTweets = 10;

/**
 * width and height
 */

var width = 600;
var height = 400;

/**
 * margin
 */

var margin = 20;

/**
 * tweet styles
 */

var itemWidth = 450;
var itemHeight = 120;
var itemMargin = 10;
var itemMarginSide = 5;

var profileImageWidth = 48;
var profileImageHeight = 48;

var fontSize = 11;

var textOffsetTop = 5 + 48 + 10;
var textOffsetLeft = 5 + fontSize + 20;
var textLineHeight = fontSize + 7;
var textWidth = width - 48 - 20;

/**
 * parser for created_time
 */

var format = d3.time.format('%a %b %d %X %Z %Y');

/**
 * shadow image
 */

var shadow = null;

/**
 * draw a tweet.format
 *
 * @param {Context} ctx
 * @param {element} tweet
 */

function drawTweet(ctx, tweet) {
  if (tweet.tagName !== 'TWEET') { return; }

  var user = tweet.firstChild;
  var text = user.nextSibling;
  var attr = tweet.getAttribute.bind(tweet);
  var x = parseInt(attr('x', 10));
  var y = parseInt(attr('y', 10));
  var fontColor = attr('font_color');

  ctx.globalAlpha = attr('opacity');

  // rect
  ctx.drawImage(shadow, x, y);
  ctx.strokeStyle = 'rgba(190, 143, 104, 1)';
  ctx.fillStyle = '#' + (user.getAttribute('profile_background_color'));
  canvasUtil.roundRect(ctx, x, y, itemWidth, itemHeight, 5, true);

  // profile image
  var img = new Image();
  img.src = user.getAttribute('profile_image_url');
  ctx.save();
  canvasUtil.roundRect(ctx, x + 5, y + 5, profileImageWidth, profileImageHeight, 5, false, false);
  ctx.clip();
  ctx.drawImage(img, x + 5, y + 5, profileImageWidth, profileImageHeight);
  ctx.restore();

  // screen name
  ctx.font = 'bold ' + fontSize + 'pt Helvetica';
  ctx.fillStyle = fontColor;
  ctx.fillText(user.getAttribute('screen_name'), x + textOffsetTop, y + 20);

  // created time
  ctx.font = (fontSize - 2) + 'pt Helvetica';
  ctx.fillStyle = 'rgb(125, 125, 125)';
  ctx.fillText(attr('time'), x + 400, y + 20);

  // text
  var textX = x + textOffsetTop;
  var textY = y + textOffsetLeft;

  ctx.font = fontSize + 'pt Helvetica';
  ctx.fillStyle = fontColor;

  text.children.forEach(function(line) {
    ctx.fillText(line.getAttribute('text'), textX, textY);
    textY += textLineHeight;
  });
}

/**
 * custom
 */

function custom(selection) {
  selection.each(function() {
    var root = this;
    var canvas = root.parentNode.appendChild(document.createElement('canvas'));
    var ctx = canvas.getContext('2d');

    canvas.style.position = 'absolute';
    canvas.style.top = root.offsetTop + 'px';
    canvas.style.left = root.offsetLeft + 'px';

    function redraw() {
      canvas.width = root.getAttribute('width');
      canvas.height = root.getAttribute('height');
      root.children.forEach(function(child) {        
        drawTweet(ctx, child);
      });
      setTimeout(redraw, 1000 / 20);
    }

    redraw();
  });
}

var room = d3.select('body').append('custom:tweets')
      .attr('id', 'room')
      .attr('width', width)
      .attr('height', height)
      .call(custom);

// ruler to calcurate word wrap
var rulerCanvas = document.createElement('canvas');
rulerCanvas.width = 450;
rulerCanvas.height = 120;
var ruler = rulerCanvas.getContext('2d');
ruler.font = 11 + 'pt Helvetica';

function lineBreak(text, x, y) {
  canvasUtil.lineBreak(ruler, text, x, y, textWidth, textLineHeight);
}

function redraw(data) {
  var tweets = room.selectAll('tweets').data(data, function(d) { return d.id_str; });

  var enter = tweets.enter().append("custom:tweet")
        .attr('id', function(d) { return d.id_str; })
        .attr('x', function(d) { return d.x; })
        .attr('y', function(d) { return d.y; })
        .attr('font_color', function(d) {
          var c = d.user.profile_background_color;
          return rgb(c).contrast().toString();
        })
        .attr('time', function(d) {  return relativeTime(format.parse(d.created_at)); })
        .attr('opacity', '.3');

  enter.append('custom:user')
      .attr('screen_name', function(d) { return d.user.screen_name; })
      .attr('profile_image_url', function(d) { return d.user.profile_image_url; })
      .attr('profile_background_color', function(d) { return d.user.profile_background_color; });

  enter.append('custom:text').selectAll('line')
      .data(function(d) { lineBreak(d.text, d.x + textOffsetTop, d.y + textOffsetLeft); })
    .enter().append('custom:line')
      .attr('text', function(d) { return d; });

  enter.transition()
      .duration(300)
      .attr('opacity', '1');

  tweets.exit()
      .transition().duration(300)
      .attr('opacity', '.3')
      .remove();
}

module.exports = {
  add: function(data) {
    // console.log(data);
  }
};
