
/**
 * Module dependencies.
 */

var d3 = require('d3');

/**
 * render rounded rect.
 */

exports.roundRect = function(ctx, x, y, w, h, r, fill, stroke) {
  r = r || 5;
  stroke = stroke || true;

  ctx.beginPath();
  ctx.moveTo(x + r, y);
  ctx.lineTo(x + w - r, y);
  ctx.quadraticCurveTo(x + w, y, x + w, y + r);
  ctx.lineTo(x + w, y + h - r);
  ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
  ctx.lineTo(x + r, y + h);
  ctx.quadraticCurveTo(x, y + h, x, y + h - r);
  ctx.lineTo(x, y + r);
  ctx.quadraticCurveTo(x, y, x + r, y);
  ctx.closePath();

  if (stroke) { ctx.stroke(); }
  if (fill) { ctx.fill(); }
};

/**
 * line break
 */

exports.lineBreak = function(ctx, text, x, y, maxWidth, lineHeight) {
  var l = '';
  var n = 0;
  var lines = [];
  var testLine, metrics, testWidth;

  while (n < text.length) {
    testLine = l + text[n];
    metrics = ctx.measureText(testLine);
    testWidth = metrics.width;
    if (testWidth > maxWidth && n > 0) {
      lines.push(l);
      l = text[n];
    } else {
      l = testLine;
    }
    n += 1;
  }
  lines.push(l);
  return lines;
};
