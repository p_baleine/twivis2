
/**
 * Module dependencies.
 */

var server = node('twivis-server');
var canvas = require('canvas');

server()
  .query('#nodejs')
  .on('data', canvas.add);

